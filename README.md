# Manusquare

![](img/Manusquared.png)

![](img/Manusquared-specimen01.png)

![](img/Manusquared-specimen02.png)

Manusquared est une famille de caractère typographique sous licence libre. Son dessin se construit autour de l'interprétation computationnelle d'une écriture manuscrite. Tracé à l'aide d'un outil de dessin vectoriel  — <a href="https://gitlab.com/bonjour-monde/tools/magnet">Magnet Mike</a> réalisé par le collectif <a href="http://www.bonjourmonde.net/">Bonjour Monde</a>. Chaque lettre est contrainte de prendre forme dans une grille magnétique. L'imperfection du geste manuel et la rencontre d'une grille stricte créent un heureux resultat.

Manusquared est dessiné par Quentin Juhel et diffusé sous licence SIL Open Font License. Si vous rencontrez des erreurs ou souhaitez ajouter des modifications, vous pouvez « forker » ce projet ou créer une « issue ».

# Designer

Quentin Juhel 
[juhel-quentin.fr](https://juhel-quentin.fr)
[contact@juhel-quentin.fr](contact@juhel-quentin.fr)

# License

Manusquared is licensed under the SIL Open Font License v1.1 (http://scripts.sil.org/OFL)

To view the copyright and specific terms and conditions please refer to [OFL.txt](https://github.com/weiweihuanghuang/Work-Sans/blob/master/OFL.txt).